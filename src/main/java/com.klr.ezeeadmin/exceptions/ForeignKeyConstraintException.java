package com.klr.ezeeadmin.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class ForeignKeyConstraintException extends RuntimeException {

    public ForeignKeyConstraintException(String msg) {
        super(msg);
    }
}
