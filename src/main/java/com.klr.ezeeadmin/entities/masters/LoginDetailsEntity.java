package com.klr.ezeeadmin.entities.masters;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.klr.ezeeadmin.entities.masters.employee.EmployeeBaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@JsonPropertyOrder({
        "user",
        "start_time",
        "end_time"
})
@Entity
@Table(name = "login_details")
public class LoginDetailsEntity {

    @Id
    String id; // Actually don't need this one but Hibernate refuse to create a table without the primary key;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "emp_login"))
    private EmployeeBaseEntity employeeBaseEntity;

    @NotNull
    private LocalDateTime startDate;

    private LocalDateTime endDate;


    public EmployeeBaseEntity getEmployeeBaseEntity() {
        return employeeBaseEntity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmployeeBaseEntity(EmployeeBaseEntity employeeBaseEntity) {
        this.employeeBaseEntity = employeeBaseEntity;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

}
