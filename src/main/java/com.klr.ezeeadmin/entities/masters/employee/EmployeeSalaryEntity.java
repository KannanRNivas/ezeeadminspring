package com.klr.ezeeadmin.entities.masters.employee;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

@Entity
@Table(name = "m_employee_salary")
public class EmployeeSalaryEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "ctc_employee_fk"), nullable = false, unique = true)
    EmployeeBaseEntity employeeBase;

    // Account Details
    String pfAccountNumber;
    String bankAccountNumber;


    //<editor-fold desc="Cost To Company">

    // Creates the database field with this size.
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Column(nullable = false, precision = 7, scale = 2)
    // Validates data when used as a form.
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcBasic;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Column(nullable = false, precision = 7, scale = 2)
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcHra;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Column(nullable = false, precision = 7, scale = 2)
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcConveyance;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Column(nullable = false, precision = 7, scale = 2)
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcMedicalAllowance;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Column(nullable = false, precision = 7, scale = 2)
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcLta;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Column(nullable = false, precision = 7, scale = 2)
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcOtherAllowance;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Column(nullable = false, precision = 7, scale = 2)
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcPerformanceBonus;

    //</editor-fold>

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public void setEmployeeBase(EmployeeBaseEntity employeeBase) {
        this.employeeBase = employeeBase;
    }

    public String getPfAccountNumber() {
        return pfAccountNumber;
    }

    public void setPfAccountNumber(String pfAccountNumber) {
        this.pfAccountNumber = pfAccountNumber;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public BigDecimal getCtcBasic() {
        return ctcBasic;
    }

    public void setCtcBasic(BigDecimal ctcBasic) {
        this.ctcBasic = ctcBasic;
    }

    public BigDecimal getCtcHra() {
        return ctcHra;
    }

    public void setCtcHra(BigDecimal ctcHra) {
        this.ctcHra = ctcHra;
    }

    public BigDecimal getCtcConveyance() {
        return ctcConveyance;
    }

    public void setCtcConveyance(BigDecimal ctcConveyance) {
        this.ctcConveyance = ctcConveyance;
    }

    public BigDecimal getCtcMedicalAllowance() {
        return ctcMedicalAllowance;
    }

    public void setCtcMedicalAllowance(BigDecimal ctcMedicalAllowance) {
        this.ctcMedicalAllowance = ctcMedicalAllowance;
    }

    public BigDecimal getCtcLta() {
        return ctcLta;
    }

    public void setCtcLta(BigDecimal ctcLta) {
        this.ctcLta = ctcLta;
    }

    public BigDecimal getCtcOtherAllowance() {
        return ctcOtherAllowance;
    }

    public void setCtcOtherAllowance(BigDecimal ctcOtherAllowance) {
        this.ctcOtherAllowance = ctcOtherAllowance;
    }

    public BigDecimal getCtcPerformanceBonus() {
        return ctcPerformanceBonus;
    }

    public void setCtcPerformanceBonus(BigDecimal ctcPerformanceBonus) {
        this.ctcPerformanceBonus = ctcPerformanceBonus;
    }
}

