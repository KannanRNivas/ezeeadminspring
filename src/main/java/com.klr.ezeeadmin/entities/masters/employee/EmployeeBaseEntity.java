package com.klr.ezeeadmin.entities.masters.employee;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.klr.ezeeadmin.entities.masters.DepartmentMasterEntity;
import com.klr.ezeeadmin.entities.masters.ShiftMasterEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.annotation.Generated;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Blob;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "m_employee_base")
public class EmployeeBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false, updatable = false)
    Long id;

    @Column(unique = true, nullable = false, updatable = false)
    @NotNull
    Long employeeId;

    @Column(nullable = false)
    @NotNull
    String name;

    @Column(nullable = false, unique = true)
    @NotNull
    @Size(max = 10)
    String shortName;

    @Column(nullable = false)
    @NotNull
    String password;

    @Lob
    Blob photo;

    @Size(max = 1)
    String gender;

    @Size(max = 10)
    String bloodGroup;

    @Size(max = 40)
    String designation;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "emp_dept_fk"))
    DepartmentMasterEntity department;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "emp_shift_fk"))
    ShiftMasterEntity shift;

    @Email
    String emailId;

    @Size(max = 15)
    String contact;

    @Column(nullable = false)
    @NotNull
    LocalDate dateOfBirth;

    @Column(nullable = false)
    @NotNull
    LocalDate dateOfJoining;

    @Size(max = 10)
    String permanentAccountNumber;

    @Size(max = 12)
    String aadharNumber;

    @Size(max = 40)
    String fatherName;

    @Size(max = 15)
    String emergencyContact;

    @Size(max = 200)
    String permanentAddress;

    @Size(max = 200)
    String residentialAddress;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Blob getPhoto() {
        return photo;
    }

    public void setPhoto(Blob photo) {
        this.photo = photo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public DepartmentMasterEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentMasterEntity department) {
        this.department = department;
    }

    public ShiftMasterEntity getShift() {
        return shift;
    }

    public void setShift(ShiftMasterEntity shift) {
        this.shift = shift;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public LocalDate getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(LocalDate dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public String getPermanentAccountNumber() {
        return permanentAccountNumber;
    }

    public void setPermanentAccountNumber(String permanentAccountNumber) {
        this.permanentAccountNumber = permanentAccountNumber;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress;
    }
}

