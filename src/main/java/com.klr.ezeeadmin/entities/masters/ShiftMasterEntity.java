package com.klr.ezeeadmin.entities.masters;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name = "m_shift")
public class ShiftMasterEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    Long id;

    @Column(name = "name", nullable = false, unique = true)
    String name;

    @Column(nullable = false)
    @JsonFormat(pattern = "HH:mm", shape = JsonFormat.Shape.STRING)
    LocalTime startTime;

    @Column(nullable = false)
    @JsonFormat(pattern = "HH:mm", shape = JsonFormat.Shape.STRING)
    LocalTime endTime;

    @Column(nullable = false)
    @JsonFormat(pattern="yyyy-MM-dd" , shape = JsonFormat.Shape.STRING)
    LocalDate startDate;

    @Column(nullable = false)
    @JsonFormat(pattern="yyyy-MM-dd" , shape = JsonFormat.Shape.STRING)
    LocalDate endDate;

    @Column(nullable = false)
    String isActive;

    @Size(max =2)
    @Column(nullable = false)
    String startEffectiveDay;

    @Size(max =2)
    @Column(nullable = false)
    String endEffectiveDay;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getStartEffectiveDay() {
        return startEffectiveDay;
    }

    public void setStartEffectiveDay(String startEffectiveDay) {
        this.startEffectiveDay = startEffectiveDay;
    }

    public String getEndEffectiveDay() {
        return endEffectiveDay;
    }

    public void setEndEffectiveDay(String endEffectiveDay) {
        this.endEffectiveDay = endEffectiveDay;
    }
}