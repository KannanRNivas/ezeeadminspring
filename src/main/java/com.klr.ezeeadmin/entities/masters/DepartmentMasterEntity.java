package com.klr.ezeeadmin.entities.masters;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

//defines the order in JSON (only on response) - if not defined will be ordered alphabetically
@JsonPropertyOrder({
        "id",
        "name",
        "inCharge",
        "phoneNumber",
        "emailId",
        "organisation"
})
@Entity
@Table(name = "m_department")
public class DepartmentMasterEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    Long id;

    @Column(unique = true, nullable = false)
    @NotNull
    String name;

    String inCharge;

    @Size(max = 15)
    @Column(unique = true)
    String phoneNumber;

    @Email
    @Column(unique = true)
    String emailId;

    String organisation; // Will change this field to organisation after OrganisationEntity Created. Need to add @ManyToOne Relation

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getInCharge() {
        return inCharge;
    }

    public void setInCharge(String inCharge) {
        this.inCharge = inCharge;
    }
}
