package com.klr.ezeeadmin.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "machine_attendance")
public class MachineAttendance {

    @Id
    @GeneratedValue
    Long id;


    @Column(nullable = false)
    @NotNull
    short employeeNumber;

    @Column(nullable = false)
    @NotNull
    LocalDateTime loggedDateTime;

    String mode;

    public short getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(short employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public LocalDateTime getLoggedDateTime() {
        return loggedDateTime;
    }

    public void setLoggedDateTime(LocalDateTime loggedDateTime) {
        this.loggedDateTime = loggedDateTime;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
