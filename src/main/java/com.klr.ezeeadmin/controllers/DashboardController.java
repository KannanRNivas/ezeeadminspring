package com.klr.ezeeadmin.controllers;

import com.klr.ezeeadmin.services.DashboardService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Dashboard")
public class DashboardController {

  @Autowired
  DashboardService dashboardService;

  @GetMapping("/details/{userId}")
  public String getDashboardDetails(@PathVariable(name ="userId") String userId){

    return dashboardService.getDashboardDetails(userId).toString();

  }


}
