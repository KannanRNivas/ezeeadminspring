package com.klr.ezeeadmin.controllers.masters;

import com.klr.ezeeadmin.entities.masters.ShiftMasterEntity;
import com.klr.ezeeadmin.services.masters.ShiftMasterService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@RestController
@RequestMapping("shift")
public class ShiftMasterController {

    @Autowired
    ShiftMasterService shiftMasterService;

    @ApiOperation(value = "Create shift master")
    @PostMapping()
    public ResponseEntity<Void> createShiftMaster(@RequestBody ShiftMasterEntity shiftMasterEntity, UriComponentsBuilder uriBuilder) {
        Long id = shiftMasterService.create(shiftMasterEntity);
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(uriBuilder.path("/shift/{id}").buildAndExpand(id).toUri());
        return new ResponseEntity<>(httpHeaders, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    Optional<ShiftMasterEntity> getShiftMaster(@PathVariable(name = "id") Long id) {
        return shiftMasterService.read(id);
    }

    @ApiOperation(value = "Update shift master")
    @PutMapping("/{id}")
    ResponseEntity<Void> updateShiftMaster(@PathVariable(name = "id") Long id, @RequestBody ShiftMasterEntity shiftMasterEntity) {
        if (shiftMasterService.update(id, shiftMasterEntity)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteShiftMaster(@PathVariable Long id) {
        if (shiftMasterService.delete(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/shiftName")
    public ResponseEntity<String> getShiftMasterName() {
        return new ResponseEntity<>(shiftMasterService.getShiftMasterCombo().toString(), HttpStatus.OK);
    }
}
