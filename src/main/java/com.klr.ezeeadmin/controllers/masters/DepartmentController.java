package com.klr.ezeeadmin.controllers.masters;

import com.klr.ezeeadmin.entities.masters.DepartmentMasterEntity;
import com.klr.ezeeadmin.services.masters.DepartmentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@RestController
@RequestMapping("departments")
public class DepartmentController {


    @Autowired
    DepartmentService departmentService;


    @ApiOperation(value = "Create a department")
    @PostMapping()
    public ResponseEntity<?> create(@RequestBody DepartmentMasterEntity departmentMasterEntity,
                                    UriComponentsBuilder ucBuilder) {
        Long id = departmentService.create(departmentMasterEntity);

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ucBuilder.path("/department/{id}").buildAndExpand(id).toUri());
        return new ResponseEntity<>(httpHeaders, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public Optional<DepartmentMasterEntity> read(@PathVariable Long id) {
        return departmentService.read(id);
    }

    @GetMapping()
    public Iterable<DepartmentMasterEntity> readAll() {
        return departmentService.readAll();
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody DepartmentMasterEntity departmentMasterEntity) {
        if (departmentService.update(id, departmentMasterEntity)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(Long id) {
        if (departmentService.delete(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
