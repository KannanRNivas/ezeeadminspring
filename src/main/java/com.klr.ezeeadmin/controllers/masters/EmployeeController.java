package com.klr.ezeeadmin.controllers.masters;

import com.klr.ezeeadmin.models.combos.EmployeeComboModel;
import com.klr.ezeeadmin.models.masters.EmployeeModel;
import com.klr.ezeeadmin.services.masters.EmployeeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@RestController
@RequestMapping("employees")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @ApiOperation(value = "Create an employee")
    @PostMapping()
    public ResponseEntity<?> create(@RequestBody EmployeeModel employeeModel,
                                    UriComponentsBuilder ucBuilder) {
        Long empId = employeeService.create(employeeModel);

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ucBuilder.path("/employees/{id}").buildAndExpand(empId).toUri());
        return new ResponseEntity<>(httpHeaders, HttpStatus.CREATED);
    }

    @GetMapping("/{employeeId}")
    public Optional<EmployeeModel> read(@PathVariable String employeeId) {
        return employeeService.readByEmployeeId(employeeId);
    }

    @GetMapping("/read-all-for-combo")
    public List<EmployeeComboModel> readAllForCombo() {
        return employeeService.readAllForCombo();
    }
//
    @PutMapping("/{employeeId}")
    public ResponseEntity<?> update(@PathVariable String employeeId, @RequestBody EmployeeModel employeeModel) {
        assert employeeId == employeeModel.getId().toString();
        if (employeeService.update(employeeModel)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
//
//    @DeleteMapping("/{employeeId}")
//    public ResponseEntity<?> delete(String employeeId) {
//        if (employeeService.delete(employeeId)) {
//            return new ResponseEntity<>(HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }
}
