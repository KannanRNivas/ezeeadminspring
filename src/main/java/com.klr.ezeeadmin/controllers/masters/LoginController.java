package com.klr.ezeeadmin.controllers.masters;

import com.klr.ezeeadmin.models.masters.LoginRequestModel;
import com.klr.ezeeadmin.services.masters.LoginService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("login")
public class LoginController {

    @Autowired
    LoginService loginService;

    @PostMapping("/auth")
    public ResponseEntity<String> auth(@RequestBody LoginRequestModel requestModel) throws JSONException {

        Long res = loginService.saveLoginDeatilsIfExist(requestModel);
        JSONObject json = new JSONObject();
        json.put("empKey",res);
        return new ResponseEntity(json.toString(), res.equals(0) ? HttpStatus.UNAUTHORIZED : HttpStatus.OK);




    }


}

