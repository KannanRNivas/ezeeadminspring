package com.klr.ezeeadmin.models.masters;

import java.time.LocalDateTime;

public class LoginRequestModel {

    private String userName;

    private String password;

    private LocalDateTime startDate;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }
}
