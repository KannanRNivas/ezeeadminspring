package com.klr.ezeeadmin.models.masters;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.klr.ezeeadmin.entities.masters.DepartmentMasterEntity;
import com.klr.ezeeadmin.entities.masters.ShiftMasterEntity;
import com.klr.ezeeadmin.models.combos.DepartmentComboModel;
import com.klr.ezeeadmin.models.combos.ShiftComboModel;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Blob;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class EmployeeModel {

    Long id;

    Long employeeId;

    @NotNull
    String name;

    @NotNull
    @Size(max = 10)
    String shortName;

    @NotNull
    String password;

    Blob photo;

    @Size(max = 1)
    String gender;

    @Size(max = 10)
    String bloodGroup;

    @Size(max = 40)
    String designation;

    DepartmentMasterEntity department;
    ShiftMasterEntity shift;

    @Email
    String emailId;

    @Size(max = 15)
    String contact;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd" , shape = JsonFormat.Shape.STRING)
    LocalDate dateOfBirth;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd" , shape = JsonFormat.Shape.STRING)
    LocalDate dateOfJoining;

    @Size(max = 10)
    String permanentAccountNumber;

    @Size(max = 12)
    String aadharNumber;

    @Size(max = 40)
    String fatherName;

    @Size(max = 15)
    String emergencyContact;

    @Size(max = 200)
    String permanentAddress;

    @Size(max = 200)
    String residentialAddress;

    // Account Details
    String pfAccountNumber;
    String bankAccountNumber;

    // Cost To Company
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcBasic;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcHra;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcConveyance;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcMedicalAllowance;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcLta;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcOtherAllowance;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#######.##")
    @Digits(integer = 9, fraction = 2)
    BigDecimal ctcPerformanceBonus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Blob getPhoto() {
        return photo;
    }

    public void setPhoto(Blob photo) {
        this.photo = photo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public DepartmentMasterEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentMasterEntity department) {
        this.department = department;
    }

    public ShiftMasterEntity getShift() {
        return shift;
    }

    public void setShift(ShiftMasterEntity shift) {
        this.shift = shift;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public LocalDate getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(LocalDate dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public String getPermanentAccountNumber() {
        return permanentAccountNumber;
    }

    public void setPermanentAccountNumber(String permanentAccountNumber) {
        this.permanentAccountNumber = permanentAccountNumber;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress;
    }

    public String getPfAccountNumber() {
        return pfAccountNumber;
    }

    public void setPfAccountNumber(String pfAccountNumber) {
        this.pfAccountNumber = pfAccountNumber;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public BigDecimal getCtcBasic() {
        return ctcBasic;
    }

    public void setCtcBasic(BigDecimal ctcBasic) {
        this.ctcBasic = ctcBasic;
    }

    public BigDecimal getCtcHra() {
        return ctcHra;
    }

    public void setCtcHra(BigDecimal ctcHra) {
        this.ctcHra = ctcHra;
    }

    public BigDecimal getCtcConveyance() {
        return ctcConveyance;
    }

    public void setCtcConveyance(BigDecimal ctcConveyance) {
        this.ctcConveyance = ctcConveyance;
    }

    public BigDecimal getCtcMedicalAllowance() {
        return ctcMedicalAllowance;
    }

    public void setCtcMedicalAllowance(BigDecimal ctcMedicalAllowance) {
        this.ctcMedicalAllowance = ctcMedicalAllowance;
    }

    public BigDecimal getCtcLta() {
        return ctcLta;
    }

    public void setCtcLta(BigDecimal ctcLta) {
        this.ctcLta = ctcLta;
    }

    public BigDecimal getCtcOtherAllowance() {
        return ctcOtherAllowance;
    }

    public void setCtcOtherAllowance(BigDecimal ctcOtherAllowance) {
        this.ctcOtherAllowance = ctcOtherAllowance;
    }

    public BigDecimal getCtcPerformanceBonus() {
        return ctcPerformanceBonus;
    }

    public void setCtcPerformanceBonus(BigDecimal ctcPerformanceBonus) {
        this.ctcPerformanceBonus = ctcPerformanceBonus;
    }
}