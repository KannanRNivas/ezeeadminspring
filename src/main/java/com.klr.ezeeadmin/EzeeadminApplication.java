package com.klr.ezeeadmin;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
@EnableScheduling
public class EzeeadminApplication {

    public static void main(String args[]) {
        SpringApplication.run(EzeeadminApplication.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
