package com.klr.ezeeadmin.services;

import com.klr.ezeeadmin.entities.MachineAttendance;
import com.klr.ezeeadmin.repos.MachineAttendanceRepo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class DashboardService {

    @Autowired
    MachineAttendanceRepo machineAttendanceRepo;

    public JSONObject getDashboardDetails(String userId) {
        LocalDate localDate = LocalDate.now();
        int month = localDate.getMonthValue();
        int year = localDate.getYear();
        List<MachineAttendance> machineAttendances = machineAttendanceRepo.findMachineAttendance(userId, month, year);


        return getTotalLeaveDetails(machineAttendances);
    }

    private JSONObject getTotalLeaveDetails(List<MachineAttendance> machineAttendances) {
        LocalDate localDate = LocalDate.now();
        AtomicInteger leave_count = new AtomicInteger(0);
        JSONArray leaveDates = new JSONArray();


        for (int i = 1; i <= localDate.lengthOfMonth(); i++) {
            LocalDate localDate1 = LocalDate.of(localDate.getYear(), localDate.getMonthValue(), i);
            if (localDate1.getDayOfWeek() != DayOfWeek.SUNDAY &&
                    localDate1.isBefore(LocalDate.now().plusDays(1))) {
                List<MachineAttendance> filteredMachine = machineAttendances
                        .stream()
                        .filter(atte -> atte.getLoggedDateTime().toLocalDate() == localDate1)
                        .sorted(Comparator.comparing(MachineAttendance::getLoggedDateTime))
                        .collect(Collectors.toList());

                if (filteredMachine.isEmpty()) {
                    leave_count.incrementAndGet();
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("Leave_date", localDate1.toString());
                        leaveDates.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


            }
        }

        JSONObject retunvalues = new JSONObject();
        try {
            retunvalues.put("LeaveCount", leave_count.get());
            retunvalues.put("LeaveDates", leaveDates);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return retunvalues;
    }

}
