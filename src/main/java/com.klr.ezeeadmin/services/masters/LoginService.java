package com.klr.ezeeadmin.services.masters;

import com.klr.ezeeadmin.entities.masters.employee.EmployeeBaseEntity;
import com.klr.ezeeadmin.entities.masters.LoginDetailsEntity;
import com.klr.ezeeadmin.models.masters.LoginRequestModel;
import com.klr.ezeeadmin.repos.masters.employee.EmployeeBaseRepo;
import com.klr.ezeeadmin.repos.masters.LoginDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;


@Service
public class LoginService {


    @Autowired
    EmployeeBaseRepo employeeBaseRepo;

    @Autowired
    LoginDetailsRepo loginDetailsRepo;


    public Long saveLoginDeatilsIfExist(LoginRequestModel loginRequestModel) {
        EmployeeBaseEntity employeeBaseEntity = employeeBaseRepo.findEmployee(loginRequestModel.getUserName(), loginRequestModel.getPassword());
        if(employeeBaseEntity != null){
            Long employeeId = employeeBaseEntity.getEmployeeId();
            LoginDetailsEntity loginEntity = new LoginDetailsEntity();
            loginEntity.setEmployeeBaseEntity(employeeBaseEntity);
            loginEntity.setStartDate(LocalDateTime.now());
            loginEntity.setId(UUID.randomUUID().toString());
            loginDetailsRepo.save(loginEntity);
            return employeeId;
        }

        return Long.valueOf(0);
    }


}
