package com.klr.ezeeadmin.services.masters;

import com.klr.ezeeadmin.entities.masters.DepartmentMasterEntity;
import com.klr.ezeeadmin.repos.masters.DepartmentMasterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DepartmentService {
    @Autowired
    DepartmentMasterRepo departmentMasterRepo;

    public Long create(DepartmentMasterEntity departmentMasterEntity) {
        DepartmentMasterEntity masterEntity = departmentMasterRepo.save(departmentMasterEntity);
        return masterEntity.getId();
    }

    public Optional<DepartmentMasterEntity> read(Long id) {
        return departmentMasterRepo.findById(id);
    }

    public Iterable<DepartmentMasterEntity> readAll() {
        return departmentMasterRepo.findAll();
    }

    public Boolean update(Long id, DepartmentMasterEntity departmentMasterEntity) {
        departmentMasterEntity.setId(id);
        departmentMasterRepo.save(departmentMasterEntity);
        return true;
    }

    public Boolean delete(Long id) {
        departmentMasterRepo.deleteById(id);
        return true;
    }

}
