package com.klr.ezeeadmin.services.masters;

import com.klr.ezeeadmin.entities.masters.ShiftMasterEntity;
import com.klr.ezeeadmin.repos.masters.ShiftMasterRepo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ShiftMasterService {
    @Autowired
    ShiftMasterRepo shiftMasterRepo;

    public Long create(ShiftMasterEntity shiftMasterEntity) {
        ShiftMasterEntity masterEntity = shiftMasterRepo.save(shiftMasterEntity);
        return masterEntity.getId();
    }

    public Optional<ShiftMasterEntity> read(Long id) {
        return shiftMasterRepo.findById(id);
    }

    public Boolean update(Long id, ShiftMasterEntity shiftMasterEntity) {
        shiftMasterEntity.setId(id);
        shiftMasterRepo.save(shiftMasterEntity);
        return true;
    }

    public Boolean delete(Long id) {
        shiftMasterRepo.deleteById(id);
        return true;
    }


    public JSONArray getShiftMasterCombo() {

        JSONArray shiftmaster = new JSONArray();
        shiftMasterRepo.findAll().iterator().forEachRemaining(shift -> {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id", shift.getId());
                jsonObject.put("name", shift.getName());
                shiftmaster.put(jsonObject);

            } catch (JSONException ex) {

            }


        });
        return shiftmaster;
    }
}
