package com.klr.ezeeadmin.services.masters;

import com.klr.ezeeadmin.entities.masters.DepartmentMasterEntity;
import com.klr.ezeeadmin.entities.masters.ShiftMasterEntity;
import com.klr.ezeeadmin.entities.masters.employee.EmployeeBaseEntity;
import com.klr.ezeeadmin.entities.masters.employee.EmployeeSalaryEntity;
import com.klr.ezeeadmin.exceptions.DataInUseException;
import com.klr.ezeeadmin.models.combos.DepartmentComboModel;
import com.klr.ezeeadmin.models.combos.EmployeeComboModel;
import com.klr.ezeeadmin.models.combos.ShiftComboModel;
import com.klr.ezeeadmin.models.masters.EmployeeModel;
import com.klr.ezeeadmin.repos.masters.employee.EmployeeBaseRepo;
import com.klr.ezeeadmin.repos.masters.employee.EmployeeSalaryRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    EmployeeBaseRepo employeeBaseRepo;
    @Autowired
    EmployeeSalaryRepo employeeSalaryRepo;

    @Autowired
    DepartmentService departmentService;
    @Autowired
    ShiftMasterService shiftMasterService;
    @Autowired
    ModelMapper modelMapper;



//    public Long create(@NotNull final EmployeeModel employeeModel) {
//
//        // Base Details
//        EmployeeBaseEntity employeeBaseEntity = modelMapper.map(employeeModel, EmployeeBaseEntity.class);
//
//        employeeBaseEntity.setId(Long.valueOf(0));
//        EmployeeBaseEntity newEmployeeBaseEntity = employeeBaseRepo.save(employeeBaseEntity);
//
//        // Salary
//
//        EmployeeSalaryEntity employeeSalaryEntity = modelMapper.map(employeeModel, EmployeeSalaryEntity.class);
//
//        employeeSalaryEntity.setId(Long.valueOf(0));
//        employeeSalaryEntity.setEmployeeBase(newEmployeeBaseEntity);
//        EmployeeSalaryEntity newEmployeeSalaryEntity = employeeSalaryRepo.save(employeeSalaryEntity);
//
//        return newEmployeeBaseEntity.getId();
//    }

    /**
     * Creates employee base and salary details
     *
     * @param employeeModel
     * @return
     */
    public Long create(@NotNull final EmployeeModel employeeModel) {

        // Base Details
        EmployeeBaseEntity employeeBaseEntity = modelMapper.map(employeeModel, EmployeeBaseEntity.class);

        Long shiftId = Optional.ofNullable(employeeModel.getShift()).map(ShiftMasterEntity::getId).orElse(Long.valueOf(0));
        Optional<ShiftMasterEntity> shiftMasterEntity = shiftMasterService.read(shiftId);
        employeeBaseEntity.setShift(shiftMasterEntity.get());

        Long departmentId = Optional.ofNullable(employeeModel.getDepartment()).map(DepartmentMasterEntity::getId).orElse(Long.valueOf(0));
        Optional<DepartmentMasterEntity> departmentMasterEntity = departmentService.read(departmentId);
        employeeBaseEntity.setDepartment(departmentMasterEntity.get());

        if (employeeBaseRepo.getMaxEmpId().equals(0)) {
            employeeBaseEntity.setEmployeeId(Long.valueOf(1));
        } else {
            Integer empId = employeeBaseRepo.getMaxEmpId();
            empId = empId + 1;
            employeeBaseEntity.setEmployeeId(Long.parseLong(String.valueOf(Long.parseLong(empId.toString()))));
        }
        employeeBaseEntity.setId(Long.valueOf(0));
        EmployeeBaseEntity newEmployeeBaseEntity = employeeBaseRepo.save(employeeBaseEntity);

        // Salary

        EmployeeSalaryEntity employeeSalaryEntity = modelMapper.map(employeeModel, EmployeeSalaryEntity.class);

        employeeSalaryEntity.setId(Long.valueOf(0));
        employeeSalaryEntity.setEmployeeBase(newEmployeeBaseEntity);
        EmployeeSalaryEntity newEmployeeSalaryEntity = employeeSalaryRepo.save(employeeSalaryEntity);

        return newEmployeeBaseEntity.getEmployeeId();
    }
//
//    /**
//     * Reads the employee base and salary details based on the given employee id
//     *
//     * @param employeeId
//     * @return
//     */
    public Optional<EmployeeModel> readByEmployeeId(@NotNull final String employeeId) {

        Optional<EmployeeBaseEntity> employeeBaseEntity = employeeBaseRepo.findByEmployeeId(employeeId);

        if (!employeeBaseEntity.isPresent()) {
            return Optional.ofNullable(null);
        }

        return Optional.ofNullable(read(employeeBaseEntity.get().getId()));
    }
//
//    /**
//     * Reads the employee base and salary details based on the given employee name
//     *
//     * @param employeeName
//     * @return
//     */
//    public Optional<EmployeeModel> readByEmployeeName(@NotNull final String employeeName) {
//
//        Optional<EmployeeBaseEntity> employeeBaseEntity = employeeBaseRepo.findByName(employeeName);
//
//        if (!employeeBaseEntity.isPresent()) {
//            return Optional.ofNullable(null);
//        }
//
//        return Optional.ofNullable(read(employeeBaseEntity.get().getId()));
//    }
//
//    /**
//     * Reads the employee base and salary details based on the given id
//     * <p>
//     * THIS METHODS SHOULD BE PRIVATE/PROTECTED AS IT IS USING INTERNAL PRIMARY KEY ID
//     *
//     * @param employeeBaseId
//     * @return
//     */
    protected EmployeeModel read(@NotNull final Long employeeBaseId) {

        Optional<EmployeeBaseEntity> employeeBaseEntity = employeeBaseRepo.findById(employeeBaseId);
        Optional<EmployeeSalaryEntity> employeeSalaryEntity = employeeSalaryRepo.findByEmployeeBase(employeeBaseEntity.get());

        EmployeeModel employeeModel = modelMapper.map(employeeSalaryEntity.get(), EmployeeModel.class);
        modelMapper.map(employeeBaseEntity.get(), employeeModel);

        if (employeeBaseEntity.get().getShift() != null) {
            ShiftMasterEntity shiftMasterEntity = new ShiftMasterEntity();
            shiftMasterEntity.setId(employeeBaseEntity.get().getShift().getId());
            shiftMasterEntity.setName(employeeBaseEntity.get().getShift().getName());
            employeeModel.setShift(shiftMasterEntity);
        } else {
            employeeModel.setShift(null);
        }
        if (employeeBaseEntity.get().getDepartment() != null) {
            DepartmentMasterEntity departmentMasterEntity = new DepartmentMasterEntity();
            departmentMasterEntity.setId(employeeBaseEntity.get().getDepartment().getId());
            departmentMasterEntity.setName(employeeBaseEntity.get().getDepartment().getName());
            employeeModel.setDepartment(departmentMasterEntity);
        } else {
            employeeModel.setDepartment(null);
        }

        return employeeModel;
    }
//
//    /**
//     * Reads all the available employees
//     *
//     * @return
//     */
//    public Iterable<EmployeeModel> readAll() {
//        // TODO
//        return null;
//    }
//
    /**
     * Reads all the available employees to load the combo
     *
     * @return
     */
    public List<EmployeeComboModel> readAllForCombo() {

        Iterable<EmployeeBaseEntity> allBaseDetails = employeeBaseRepo.findAll();
        List<EmployeeComboModel> allEmployeeComboModels = new ArrayList<>();
        allBaseDetails.forEach(employeeBaseEntity -> {
            EmployeeComboModel employeeComboModel = new EmployeeComboModel();
            employeeComboModel.setId(employeeBaseEntity.getId());
            employeeComboModel.setName(employeeBaseEntity.getName());
            employeeComboModel.setShortName(employeeBaseEntity.getShortName());
            allEmployeeComboModels.add(employeeComboModel);
        });

        return allEmployeeComboModels;
    }
//
//    /**
//     * Update the given employee model
//     *
//     * @param employeeModel
//     * @return
//     */
    public Boolean update(@NotNull final EmployeeModel employeeModel) {
        Optional<EmployeeBaseEntity> oldEmployeeBaseEntity = employeeBaseRepo.findByEmployeeId(employeeModel.getId().toString());
        if (!oldEmployeeBaseEntity.isPresent()) {
            return false;
        } else {
            Optional<EmployeeSalaryEntity> oldEmployeeSalaryEntity = employeeSalaryRepo.findByEmployeeBase(oldEmployeeBaseEntity.get());


            // Base Details
            EmployeeBaseEntity employeeBaseEntity = modelMapper.map(employeeModel, EmployeeBaseEntity.class);

            Long shiftId = Optional.ofNullable(employeeModel.getShift()).map(ShiftMasterEntity::getId).orElse(Long.valueOf(0));
            Optional<ShiftMasterEntity> shiftMasterEntity = shiftMasterService.read(shiftId);
            employeeBaseEntity.setShift(shiftMasterEntity.get());

            Long departmentId = Optional.ofNullable(employeeModel.getDepartment()).map(DepartmentMasterEntity::getId).orElse(Long.valueOf(0));
            Optional<DepartmentMasterEntity> departmentMasterEntity = departmentService.read(departmentId);
            employeeBaseEntity.setDepartment(departmentMasterEntity.get());

            employeeBaseEntity.setId(oldEmployeeBaseEntity.get().getId());
            EmployeeBaseEntity newEmployeeBaseEntity = employeeBaseRepo.save(employeeBaseEntity);

            // Salary

            EmployeeSalaryEntity employeeSalaryEntity = modelMapper.map(employeeModel, EmployeeSalaryEntity.class);

            employeeSalaryEntity.setId(oldEmployeeSalaryEntity.get().getId());
            employeeSalaryEntity.setEmployeeBase(newEmployeeBaseEntity);
            EmployeeSalaryEntity newEmployeeSalaryEntity = employeeSalaryRepo.save(employeeSalaryEntity);

            return true;
        }


    }
//
//    /**
//     * Delete the given employee
//     *
//     * @param employeeId
//     * @return
//     */
//    public Boolean delete(@NotNull final String employeeId) {
//
//        Optional<EmployeeBaseEntity> oldEmployeeBaseEntity = employeeBaseRepo.findByEmployeeId(employeeId);
//        if (!oldEmployeeBaseEntity.isPresent()) {
//            return false;
//        } else {
//            // Check if employee used anywhere
//            if (false) { // if used
////                throw new ForeignKeyConstraintException("Your Message");  // only if used by foreign key tables
//                throw new DataInUseException("Your Message");
//            } else { //if not used
//                Optional<EmployeeSalaryEntity> oldEmployeeSalaryEntity = employeeSalaryRepo.findByEmployeeBase(oldEmployeeBaseEntity.get());
//                employeeSalaryRepo.deleteById(oldEmployeeSalaryEntity.get().getId());
//                employeeBaseRepo.deleteById(oldEmployeeBaseEntity.get().getId());
//            }
//        }
//        return true;
//    }
}
