package com.klr.ezeeadmin.services;

import com.klr.ezeeadmin.entities.MachineAttendance;
import com.klr.ezeeadmin.repos.MachineAttendanceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import smack.comm.SBXPCProxy;
import smack.comm.output.GeneralLogDataOutput;

import java.time.LocalDateTime;


@Component
public class AttendanceService {


    @Autowired
    MachineAttendanceRepo machineAttendanceRepo;
    @Value("${ezeeadmin.machine.no:0}")
    private long MACHINE_CODE;
    @Value("${ezeeadmin.machine.ip:}")
    private String IP_ADDRESS;
    @Value("${ezeeadmin.machine.port:0}")
    private long PORT;
    @Value("${ezeeadmin.machine.password:0}")
    private long PASSWORD;
    private boolean isConnected = false;

    @Scheduled(fixedDelay = 10000)
    public void checkAttendance() {

        if (connect()) {
            boolean ret = SBXPCProxy.ReadGeneralLogData(MACHINE_CODE, true);
            if (ret) {
                GeneralLogDataOutput output;
                while (true) {
                    output = SBXPCProxy.GetGeneralLogData(MACHINE_CODE);
                    if (!output.isSuccess())
                        break;
                    constructAndSaveAttendance(output);
                }


            } else {
                System.out.println(SBXPCProxy.GetLastError(MACHINE_CODE).dwValue);
            }
        }
    }


    public boolean connect() {
        if(!isConnected){
            if (SBXPCProxy.ConnectTcpip(MACHINE_CODE, IP_ADDRESS, PORT, PASSWORD)) {
                isConnected = true;
                return true;
            } else {
                System.out.println("System not connected !!!");
                return false;
            }
        }else{
            return true;
        }

    }

    public void test(SBXPCProxy a) {
        if (a == null) {
            System.out.println("Test");
        }
    }

    private void constructAndSaveAttendance(GeneralLogDataOutput output) {
        MachineAttendance machineAttendance = new MachineAttendance();

        machineAttendance.setEmployeeNumber(Short.valueOf(String.valueOf(output.dwEnrollNumber)));
        machineAttendance.setMode(String.valueOf(output.dwVerifyMode));

        int year = Integer.parseInt(String.valueOf(output.dwYear));
        int month = Integer.parseInt(String.valueOf(output.dwMonth));
        int date = Integer.parseInt(String.valueOf(output.dwDay));
        int hours = Integer.parseInt(String.valueOf(output.dwHour));
        int min = Integer.parseInt(String.valueOf(output.dwMinute));
        int sec = Integer.parseInt(String.valueOf(output.dwSecond));
        LocalDateTime localDateTime = LocalDateTime.of(year, month, date, hours, min, sec);
        machineAttendance.setLoggedDateTime(localDateTime);

        machineAttendanceRepo.save(machineAttendance);

    }


}
