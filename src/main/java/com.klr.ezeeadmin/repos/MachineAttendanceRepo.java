package com.klr.ezeeadmin.repos;

import com.klr.ezeeadmin.entities.EmployeeEntity;
import com.klr.ezeeadmin.entities.MachineAttendance;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MachineAttendanceRepo extends CrudRepository<MachineAttendance, String> {


    @Query(value = "select * from MACHINE_ATTENDANCE " +
            "where month(logged_date_time) = ?2 " +
            "and year(logged_date_time) = ?3 " +
            "and employee_number = ?1", nativeQuery = true)
    public List<MachineAttendance> findMachineAttendance(String userId, int month , int year);

}

