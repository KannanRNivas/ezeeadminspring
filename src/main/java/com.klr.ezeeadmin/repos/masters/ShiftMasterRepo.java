package com.klr.ezeeadmin.repos.masters;

import com.klr.ezeeadmin.entities.masters.ShiftMasterEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShiftMasterRepo extends CrudRepository<ShiftMasterEntity,Long> {
}
