package com.klr.ezeeadmin.repos.masters.employee;

import com.klr.ezeeadmin.entities.masters.employee.EmployeeBaseEntity;
import com.klr.ezeeadmin.entities.masters.employee.EmployeeSalaryEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeSalaryRepo extends CrudRepository<EmployeeSalaryEntity, Long> {

    @Query(value = "select * from m_employee_salary  where employee_base_id = ?1", nativeQuery = true)
    Optional<EmployeeSalaryEntity> findByEmployeeBase(EmployeeBaseEntity employeeBase);

}

