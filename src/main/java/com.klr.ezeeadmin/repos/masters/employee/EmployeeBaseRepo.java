package com.klr.ezeeadmin.repos.masters.employee;

import com.klr.ezeeadmin.entities.masters.employee.EmployeeBaseEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeBaseRepo extends CrudRepository<EmployeeBaseEntity, Long> {

    @Query(value = "select * from m_employee_base  where id = ?1 or short_name = ?1", nativeQuery = true)
    Optional<EmployeeBaseEntity> findByEmployeeId(String employeeId);

    Optional<EmployeeBaseEntity> findByName(String name);

    @Query(value = "select * from m_employee_base  where short_name = ?1 and password = ?2", nativeQuery = true)
    public EmployeeBaseEntity findEmployee(String userName, String password);

    @Query(value = "select count(employee_id) from m_employee_base", nativeQuery = true)
    Integer getMaxEmpId();


}

