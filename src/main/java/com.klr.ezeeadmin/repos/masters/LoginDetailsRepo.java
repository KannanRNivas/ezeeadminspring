package com.klr.ezeeadmin.repos.masters;

import com.klr.ezeeadmin.entities.masters.LoginDetailsEntity;
import org.springframework.data.repository.CrudRepository;

public interface LoginDetailsRepo extends CrudRepository<LoginDetailsEntity, String> {

}
