package com.klr.ezeeadmin.repos.masters;


import com.klr.ezeeadmin.entities.masters.DepartmentMasterEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentMasterRepo extends CrudRepository<DepartmentMasterEntity, Long> {

}
